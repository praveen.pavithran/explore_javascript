num_array = [0, 1, 2, 1, 3, 1, 2]

//Detailed function 
function func (element, index, array) {
    console.log(`element: ${element} ; index: ${index}; array: ${array}`)
    return (element > 1)
}

console.log(num_array.filter(func))

// Simple function with element only
function simple_func (element) {
    return (element > 1)
}
console.log(num_array.filter(simple_func))

//Inline function
let filtered_array = num_array.filter(element =>  element> 1)
console.log(filtered_array)

//Function with variable threshold
const filternums = (array, threshold) =>{
    return array.filter( (element) => element > threshold)
}
    
console.log(filternums(num_array, 2))
console.log(filternums(num_array, 1))


// Word arrays
let words = ['spray', 'limit', 'exuberant', 'destruction', 'elite', 'present'];

const modifiedWords = words.filter((word, index, arr) => {
  
  return word.length < 6;
});

console.log(modifiedWords);