//Traditional function
function sum (a, b) {
    return (a+b)
}
console.log(sum(1,2 ))

// function assigned to a constant
const sum_const = function(a, b) {
    return (a+b)
}
console.log(sum_const(1,2))

//Arrow function
const sum_arrow_function = (a,b) => {
    return (a+b)
}
console.log(sum_arrow_function(1,2))

//Single line arrow function with single input 
const square_arrow_function= a => a*a

console.log(square_arrow_function(2))
