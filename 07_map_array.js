const array1 = [1, 4, 9, 16];

// detailed function

function func_double (element, index, array) {
    console.log(`element: ${element} ; index: ${index}; array: ${array}`)
    return 2*element
}

let double_func=array1.map(func_double)
console.log(double_func)

// Element only
function func_double_simple (element) {
    console.log(`element: ${element}`)
    return 2*element
}

double_func=array1.map(func_double_simple)
console.log(double_func)


//Inline arrow function
double_func=array1.map(element => 2*element)
console.log(double_func)