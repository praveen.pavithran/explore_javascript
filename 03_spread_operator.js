array = [2, 3, 4]

// Add new elements in the beginning of the array
prefix_array  = [0, 1, ...array]
console.log(prefix_array)

//Add new elements to the end of the array
postfix_array  = [...array, 5, 6]
console.log(postfix_array)

//Add new properties to an object
some_object ={
    name: 'Murph'
}

new_object = {
    ...some_object, newProp: 'Bark'
}

console.log(new_object)

// Spread operators for functions
function sort(...args){
    return (args.sort())
}

console.log(sort(2, 6, 4, 1, 7, 3))