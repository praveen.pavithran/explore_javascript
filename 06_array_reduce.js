const array = [10, 1, 2, 3]

// Details of how this works

function reducer(previousValue, currentValue, index) {
    const returns = previousValue + currentValue;
    console.log(`previousValue: ${previousValue}, currentValue: ${currentValue}, index: ${index}, returns: ${returns}`);
    return returns;
  }
  
  const finaVal=array.reduce(reducer);

  console.log(`finalvalue: ${finaVal}`)

  const finValueWithInitialValue = array.reduce(reducer, 20)
  console.log('finValueWithInitial', finValueWithInitialValue)

const reduceArray = array.reduce(
  
    (previousVal, currentVal)=> previousVal+currentVal
)
   
console.log(reduceArray)

// Get max of an array



const maxArray = array.reduce(
    (previousVal, currentVal) => Math.max(previousVal, currentVal) 
    
    
)

console.log(`max of array: ${maxArray}`)

const maxReducer = function (previousVal, currentVal) {
    return Math.max(previousVal, currentVal)
}

const maxFunctionArray = array.reduce(maxReducer)

console.log(maxFunctionArray)

// using single line arrow function

const getMax = (a, b) => Math.max(a, b);
console.log(`Max Single line function: ${array.reduce(getMax)}`)


// Sum values in an object
const objects = [{ x: 1 }, { x: 2 }, { x: 3 }];
const sum = objects.reduce(
  (previousValue, currentValue) => previousValue + currentValue.x,
  0,
);

console.log(`Sum of values in object: ${sum}`);

const names = ['Ada', 'Fortran', 'Go', 'Ada', 'Ada', 'Go'];

const countedNames = names.reduce((allNames, name) => {
  allNames[name] ??= 0;
  allNames[name]++;
  // Remember to return the object, or the next iteration
  // will receive undefined
  return allNames;
}, {});

console.log(`Count of names: ${JSON.stringify(countedNames)}`)


const myArrayWithNoDuplicates = names.reduce((previousValue, currentValue) => {
    if (previousValue.indexOf(currentValue) === -1) {
      previousValue.push(currentValue);
    }
    return previousValue;
  }, []);
  
  console.log(myArrayWithNoDuplicates);



// Building-blocks to use for composition
const double = (x) => 2 * x;
const triple = (x) => 3 * x;
const quadruple = (x) => 4 * x;

// Function composition enabling pipe functionality
const pipe = (...functions) => (initialValue) => functions.reduce(
  (acc, fn) => fn(acc),
  initialValue,
);

// Composed functions for multiplication of specific values
const multiply6 = pipe(double, triple);
const multiply9 = pipe(triple, triple);
const multiply16 = pipe(quadruple, quadruple);
const multiply24 = pipe(double, triple, quadruple);

// Usage
console.log(multiply6(6));   // 36
console.log(multiply9(9));   // 81
console.log(multiply16(16)); // 256
console.log(multiply24(10)); // 240